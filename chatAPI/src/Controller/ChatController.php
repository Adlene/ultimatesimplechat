<?php

namespace App\Controller;

use App\Entity\Message;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ChatController extends Controller
{
    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }
    /**
     * @Route("/chat", name="chat")
     */
    public function index()
    {
        return $this->render('chat/index.html.twig', [
            'controller_name' => 'ChatController',
        ]);
    }
    /**
     * @Route("/room", name="room")
     */
    public function room()
    {
        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository(Message::class)
            ->findAll();
        $data = $this->serializer->serialize($data, 'json');

        return new JsonResponse($data);
    }
    /**
     * @Route("/message/new", name="message", methods = "POST")
     */
    public function postMessage(Request $request)
    {

        $data = $request->getContent();
        $data = json_decode($data,true);


        $message = new Message();
        $message->setUser($data['user'])
                ->setContent($data['content']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();

        return new JsonResponse(['message' => 'Done']);

    }
}
